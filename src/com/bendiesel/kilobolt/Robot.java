package com.bendiesel.kilobolt;

import java.util.ArrayList;

import android.graphics.Rect;

public class Robot {

	final int JUMPSPEED = -15;
	final int MOVESPEED = 5;
	
	private int centerX = 100;
	private int centerY = 377;
	private final int minX = 60;
	@SuppressWarnings("unused")
	private final int maxY = 377;
	
	
	private boolean jumped = false;
	private boolean movingLeft = false;
	private boolean movingRight = false;
	private boolean ducked = false;
	private boolean readyToFire = true;

	private static Background bg1 = GameScreen.getBg1();
	private static Background bg2 = GameScreen.getBg2();
	
	private int speedX = 0;
	private int speedY = 0;

	public static Rect rect = new Rect(0,0,0,0);
	public static Rect rect2 = new Rect(0,0,0,0);
	public static Rect rect3 = new Rect(0,0,0,0);
	public static Rect rect4 = new Rect(0,0,0,0);
	public static Rect dangerZone = new Rect(0,0,0,0); 
	public static Rect footLeft = new Rect(0,0,0,0);
	public static Rect footRight = new Rect(0,0,0,0);
	
	
	private ArrayList<Projectile> projectiles = new ArrayList<Projectile>();
	
	public void update() {

		// Moves Character or scrolls
		if (speedX < 0) 
			centerX += speedX;
		
		if (speedX == 0 || speedX < 0) {
			bg1.setSpeedX(0);
			bg2.setSpeedX(0);
		} 
		
		if (centerX <= 200 && speedX > 0){
			centerX += speedX;
		}
		
		if (speedX > 0 && centerX > 200){
			bg1.setSpeedX(-MOVESPEED/5);
			bg2.setSpeedX(-MOVESPEED/5);
		}

		centerY += speedY;
		
		// Handles Jumping
		speedY += 1;
		if (speedY > 3)
			jumped = true;
			
		

		// Prevents going beyond X coordinate of 0
		if (checkXBoundary(minX)) {
			centerX = minX + 1;
		}
		
		rect.set(centerX - 34, centerY - 63, centerX + 34, centerY);
		rect2.set(rect.left, rect.top + 63, rect.left + 68, rect.top + 128);
		rect3.set(rect.left- 26, rect.top + 32, rect.left, rect.top + 52);
		rect4.set(rect.left + 68, rect.top+ 32, rect.left + 94, rect.top + 52);
		dangerZone.set(centerX-110,  centerY-110, centerX + 70, centerY + 70);
		footLeft.set(centerX - 50, centerY + 20, centerX, centerY + 35);
		footRight.set(centerX, centerY + 20, centerX +50, centerY + 35);
	}

	// Returns true if the Y-coordinate value is above 'y'
	@SuppressWarnings("unused")
	private boolean checkYBoundary(int y) {
		// TODO Auto-generated method stub
		if (centerY + speedY >= y)
			return true;
		return false;
	}

	// Returns true if the X-coordinate value is 'x' from 0
	private boolean checkXBoundary(int x) {
		// TODO Auto-generated method stub
		if (centerX + speedX <= x)
			return true;

		return false;
	}

	public void moveRight() {
		if(ducked == false)
			speedX = MOVESPEED;
	}

	public void moveLeft() {
		if(ducked == false)
			speedX = -MOVESPEED;
	}

	public void stopLeft(){
		setMovingLeft(false);
		stop();
	}
	
	public void stopRight(){
		setMovingRight(false);
		stop();
	}
	
	public void stop() {
		if(isMovingRight() == false && isMovingLeft() == false)
			speedX = 0;
		
		if(isMovingRight() == false && isMovingLeft() == true)
			moveLeft();
		
		if(isMovingRight() == true && isMovingLeft() == false)
			moveRight();
			
	}

	private boolean isMovingLeft() {
		// TODO Auto-generated method stub
		return movingLeft;
	}

	private boolean isMovingRight() {
		// TODO Auto-generated method stub
		return movingRight;
	}

	public void setMovingRight(boolean right){
		this.movingRight = right;
	}
	
	public void setMovingLeft(boolean left){
		this.movingLeft = left;
	}
	
	public void jump() {
		if (jumped == false) {
			speedY = JUMPSPEED;
			jumped = true;
		}
	}

	public void shoot(){
		if(readyToFire){
			Projectile p = new Projectile(centerX + 50, centerY -25);
			projectiles.add(p);
		}
			
	}
	
	public ArrayList<Projectile> getProjectiles(){
		return projectiles;
	}

	public int getCenterX() {
		return centerX;
	}

	public void setCenterX(int centerX) {
		this.centerX = centerX;
	}

	public void setCenterY(int centerY) {
		this.centerY = centerY;
	}

	public void setJumped(boolean jumped) {
		this.jumped = jumped;
	}


	public void setSpeedX(int speedX) {
		this.speedX = speedX;
	}

	public void setSpeedY(int speedY) {
		this.speedY = speedY;
	}
	
	public boolean isDucked(){
		return ducked;
	}
	
	public void setDucked(boolean ducked){
		this.ducked = ducked;
	}

	public int getCenterY() {
		// TODO Auto-generated method stub
		return centerY;
	}

	public boolean isJumped() {
		// TODO Auto-generated method stub
		return jumped;
	}

	public boolean isReadyToFire() {
		return readyToFire;
	}

	public void setReadyToFire(boolean readyToFire) {
		this.readyToFire = readyToFire;
	}
	
	public void dead(){
//		try{
//		jumped = false;
//		movingLeft = false;
//		movingRight = false;
//		ducked = false;
//		readyToFire = true;
//
//		speedX = 0;
//		speedY = 0;
//
//		rect = null;
//		rect2 =  null;
//		rect3 = null;
//		rect4 = null;
//		dangerZone = null;
//		footLeft = null;
//		footRight = null;
//		
//		
//		projectiles.clear();
//		return true;
//		}
//		catch(Exception e){
//			return false;
//		}
	}
	public void restart(){
//		rect = new Rectangle(0,0,0,0);
//		rect2 = new Rectangle(0,0,0,0);
//		rect3 = new Rectangle(0,0,0,0);
//		rect4 = new Rectangle(0,0,0,0);
//		dangerZone = new Rectangle(0,0,0,0); 
//		footLeft = new Rectangle(0,0,0,0);
//		footRight = new Rectangle(0,0,0,0);
//		System.out.println("speedx is " + this.speedX);
//		System.out.println("speedy is " + this.speedY);
//		centerX = 100;
//		centerY = 377;
//
//		bg1 = GameScreen.getBg1();
//		bg2 = GameScreen.getBg2();
	}

}

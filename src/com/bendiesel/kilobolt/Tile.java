package com.bendiesel.kilobolt;

import android.graphics.Rect;
import android.util.Log;

import com.kilobolt.framework.Image;

public class Tile {
	
	private int tileX, tileY, speedX;
	public int type;
	public Image tileImage;
	
	private Robot robot = GameScreen.getRobot();
	private Background bg = GameScreen.getBg1();

	private Rect r;
	
	public Tile(int x, int y, int typeInt) {
		tileX = x * 40;
		tileY = y * 40;
	
		r = new Rect();
		
		type = typeInt;
	
		if(type == 8)
			tileImage = Assets.tilegrassTop;
		else if(type == 5)
			tileImage = Assets.tiledirt;
		else if(type == 4)
			tileImage = Assets.tilegrassLeft;
		else if(type == 6)
			tileImage = Assets.tilegrassRight;
		else if(type == 2)
			tileImage = Assets.tilegrassBot;
		else
			type = 0;
	}

	public void update(){
		speedX = bg.getSpeedX() * 5;
		tileX += speedX;
		r.set(tileX, tileY, tileX+ 40, tileY + 40);
		
		if(Rect.intersects(r, Robot.dangerZone) && type != 0){
			checkVerticalCollisions(Robot.rect, Robot.rect2);
			checkSideCollision(Robot.rect3, Robot.rect4, Robot.footLeft, Robot.footRight);
			
		}
	}

	public int getTileX() {
		return tileX;
	}

	public void setTileX(int tileX) {
		this.tileX = tileX;
	}

	public int getTileY() {
		return tileY;
	}

	public void setTileY(int tileY) {
		this.tileY = tileY;
	}

	public Image getTileImage() {
		return tileImage;
	}

	public void setTileImage(Image tileImage) {
		this.tileImage = tileImage;
	}

	public void checkVerticalCollisions(Rect rtop, Rect rbot){
		if(Rect.intersects(rtop, r))
			System.out.println("upper collision");
		if(Rect.intersects(rbot, r) && type == 8){
			robot.setJumped(false);
			robot.setSpeedY(0);
			robot.setCenterY(tileY - 63);
		}
	}
	
	public void checkSideCollision(Rect rLeft, Rect rRight, 
									Rect leftFoot, Rect rightFoot){
		
		if(type != 5 && type != 2 && type != 0){
			if(Rect.intersects(rLeft, r)){
				robot.setCenterX(tileX + 102);
				robot.setSpeedX(0);
			}
			else if(Rect.intersects(leftFoot, r)){
				robot.setCenterX(tileX + 85);
				robot.setSpeedX(0);
			}
			
			if(Rect.intersects(rRight, r)){
				robot.setCenterX(tileX - 62);
				robot.setSpeedX(0);
			}
			
			else if(Rect.intersects(rightFoot, r)){
				robot.setCenterX(tileX - 45);
				robot.setSpeedX(0);
			}
		}
	}
}

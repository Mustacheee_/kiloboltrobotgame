package com.bendiesel.kilobolt;

import android.graphics.Rect;


public class Enemy {

	private int speedX, centerX, centerY, power;
	private Background bg = GameScreen.getBg1();
	private Robot robot = GameScreen.getRobot();
	public Rect r = new Rect(0,0,0,0);
	public int health = 5;
	
	private int movementSpeed;
	
	public void update() {
			follow();
			centerX += speedX;
			speedX = bg.getSpeedX() * 5 + movementSpeed;
			r.set(centerX - 25, centerY - 25, 50, 60);
			
			if(Rect.intersects(r, Robot.dangerZone))
				checkCollision();
	}

	private void follow() {
		if(centerX < -95 || centerX > 810)
			movementSpeed = 0;
		else if(Math.abs(robot.getCenterX() - centerX) < 5)
			movementSpeed = 0;
		else{
			if(robot.getCenterX() >= centerX)
				movementSpeed = 1;
			else
				movementSpeed = -1;
		}
		
	}

	private void checkCollision() {
		if(Rect.intersects(r, Robot.rect) || Rect.intersects(r, Robot.rect2) ||
				Rect.intersects(r, Robot.rect3) || Rect.intersects(r, Robot.rect4)){
			
		}
		
	}

	public void die() {
//		try{
//		speedX = 0;
//		centerX = 0;
//		centerY = 0;
//		robot = null;
//		r = null;
//		movementSpeed = 0;
//		return true;
//		}
//		catch(Exception e){
//			return false;
//		}
	}

	public void attack() {

	}

	public int getSpeedX() {
		return speedX;
	}

	public void setSpeedX(int speedX) {
		this.speedX = speedX;
	}

	public int getCenterX() {
		return centerX;
	}

	public void setCenterX(int centerX) {
		this.centerX = centerX;
	}

	public int getCenterY() {
		return centerY;
	}

	public void setCenterY(int centerY) {
		this.centerY = centerY;
	}

	public Background getBg() {
		return bg;
	}

	public void setBg(Background bg) {
		this.bg = bg;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}
	
	
	
}
